﻿using System;
using System.IO;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using RestSharp;
using Newtonsoft.Json;
using System.Threading;
using log4net;
using System.Xml;
using System.Net;
using System.Xml.Serialization;
using RestSharp.Extensions.MonoHttp;
using System.Diagnostics;
using System.Configuration;

namespace XYZContractServices
{
    [XmlRoot("exception"), XmlType("exception")]
    public class SerializableException
    {
        [XmlElement("message")]
        public string Message { get; set; }

        [XmlElement("innerException")]
        public SerializableException InnerException { get; set; }
    }

    public class RESTManifest
    {
        public string baseUri { get; set; }
        public string uriResource { get; set; }
    }

    public class APPWebServiceManifest
    {
        public string DBServer { get; set; }
        public string Database { get; set; } 
        public string APPDBUsername { get; set; }
        public string APPDBPassword { get; set; }
        public string APPUsername { get; set; }
        public string APPPassword { get; set; }
        public string APPImportString { get; set; }
    }

    public class LoanInfoView
    {
        public string FullName { get; set; }
        public string Loan_Number { get; set; }
        public string Contract_Date { get; set; }
        public string Dealer_Code { get; set; }
        public string ACH { get; set; }
        public string CifNumber { get; set; }
        public string LastName1 { get; set; }
        public string FirstName1 { get; set; }
        public string MiddleName1 { get; set; }
    }


    public class ContractManifest
    {
        public string LoanNumber { get; set; }
        public string DateRecorded { get; set; }
        public string DealerCode { get; set; }
        public string ACH { get; set; }
        public string CifNumber { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string InputContractFileName { get; set; }
        public string OutputContractFileName { get; set; }
        public string ExceptionsContractFolder { get; set; }
    }
    public class Contracts : IContracts
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Contracts));
        
        public Contracts()
        {
            configLog4net();
        }

        /// <summary>
        ///  This method configures log4net use in this dll.
        /// </summary>
        private void configLog4net()
        {

            if (!log4net.LogManager.GetRepository().Configured)
            {
                var configFile = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "\\log4net.config");

                if (!configFile.Exists)
                {
                    throw new FileLoadException(String.Format("The configuration file {0} does not exist", configFile));
                }
                log4net.Config.XmlConfigurator.Configure(configFile);
                log.Info("Setup in Application_Start - ");
                log.InfoFormat("++ Available processors (for Parallel use): {0}", System.Environment.ProcessorCount);
                log.InfoFormat("++ ContractServices.dll - Version: {0}", typeof(Contracts).Assembly.GetName().Version.ToString());
                log.InfoFormat("++ App.config - Web Service ContractInfoUrl: {0}/{1}/<loan number>", ConfigurationManager.AppSettings[@"WebServiceHost"], ConfigurationManager.AppSettings[@"WebServiceContractInfoUri"]);
                log.InfoFormat("++ App.config - ScanContractsFolder: {0}", ConfigurationManager.AppSettings[@"ScanContractsFolder"]);
                log.InfoFormat("++ App.config - ConsumerRepositoryMissingFolder: {0}", ConfigurationManager.AppSettings[@"ConsumerRepositoryMissingFolder"]);
                log.InfoFormat("++ App.config - ConsumerFiles: {0}", ConfigurationManager.AppSettings[@"ConsumerFiles"]);
                log.InfoFormat("++ App.config - File watch timer: {0}", ConfigurationManager.AppSettings[@"FileWatchTimer"]);
                log.InfoFormat("++ App.config - Database Server: {0}", ConfigurationManager.AppSettings[@"DBServer"]);
                log.InfoFormat("++ App.config - Database: {0}", ConfigurationManager.AppSettings[@"Database"]);
                log.Info("++ FUTURE ** configuration validation method executes here...");
            }
        }


        /// <summary>
        /// This method processes the finance contract including adding a tag to the right margin and storing it in the consumer's folder.
        /// </summary>
        /// <param name="rm"></param>
        /// <param name="cm"></param>
        /// <param name="nm"></param>
        /// <param name="emailCommon"></param>
        /// <param name="dataCommon"></param>
        /// <returns>Boolean</returns>
        public Boolean processFinanceContract(RESTManifest rm, ContractManifest cm, APPWebServiceManifest nm, EmailCommon emailCommon)
        {
            Boolean bStatus = false;
            Contracts cs = new Contracts();
            string metadataFile = string.Empty;
            string originalScannedFile = cm.InputContractFileName;
            string folderConsumerFiles = cm.OutputContractFileName;
            string folderConsumerExceptions = cm.ExceptionsContractFolder;
            string folderConsumerFolder = string.Empty;
            int threadID = Thread.CurrentThread.ManagedThreadId;
            string sResponse = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            string[] listConsumerFolders = null;
            int folderCount = 0;
            
            
            log.InfoFormat("File {0} is being processed in task {1} ", cm.InputContractFileName, threadID);
            log.InfoFormat("REST Service is located at {0} using resource {1}", rm.baseUri, rm.uriResource);
            try
            {
                cm.LoanNumber = Path.GetFileNameWithoutExtension(originalScannedFile);
                cm = cs.getLoanInfo(rm, cm.LoanNumber);
                cm.InputContractFileName = originalScannedFile;
                cm.OutputContractFileName = folderConsumerFiles;
                cm.ExceptionsContractFolder = folderConsumerExceptions;

                if (File.Exists(cm.InputContractFileName))
                {
                    listConsumerFolders = FindConsumerInFolder(folderConsumerFiles, cm.LastName, cm.FirstName, cm.DealerCode);
                    if (listConsumerFolders == null)
                    {
                        folderCount = 0;
                        processException(folderCount, cm, emailCommon);
                    }
                    else if (listConsumerFolders.Length == 1)
                    {
                        folderConsumerFolder = listConsumerFolders[0];
                        log.DebugFormat("Consumer's folder located at {0} for loan {1}.", folderConsumerFolder, cm.LoanNumber);

                        //Add metadata to the pdf file
                        cm.OutputContractFileName = string.Format(@"{0}\\{1}_METADATA{2}", folderConsumerFolder, Path.GetFileNameWithoutExtension(cm.InputContractFileName), Path.GetExtension(cm.InputContractFileName));
                        metadataFile = cs.AddMetadataPDF(cm);
                        cm.InputContractFileName = metadataFile;

                        //Add a stamp to the pdf file
                        cm.OutputContractFileName = string.Format(@"{0}\\{1}v{2}{3}", folderConsumerFolder, Path.GetFileNameWithoutExtension(cm.InputContractFileName.Replace("_METADATA", string.Empty)), GetDayTimestamp(), Path.GetExtension(cm.InputContractFileName));
                        cs.InsertTextToPdf(cm);
                        File.Delete(cm.InputContractFileName);
                        log.DebugFormat("Processing is complete - removed temp file: {0}", cm.InputContractFileName);
                        File.Delete(originalScannedFile);
                        log.DebugFormat("Processing is complete - removed original file: {0}", originalScannedFile);
                        log.InfoFormat("File processing complete. File moved from {0} to {1}", cm.InputContractFileName, cm.OutputContractFileName);
                        UpdateAPP(nm, cm.LoanNumber);
                    }
                    else
                    {
                        folderCount = listConsumerFolders.Length;
                        processException(folderCount, cm, emailCommon);
                    }
                }
                else
                {
                    log.InfoFormat("File to be processed was not found -  {0}", cm.OutputContractFileName);
                    bStatus = true;
                }
            }
            catch (IOException ioe)
            {
                log.ErrorFormat("Error occurred during handling of consumer file exception - {0}. Exception: {1}", cm.InputContractFileName, ioe.Message);
                bStatus = true;
            }
            catch (Exception ex)
            {
                StackFrame callStack = new StackFrame(1, true);
                log.ErrorFormat("FAILURE: An exception occurred during the processing of the consumer file. Exception: {0} at line {1}", ex.Message, callStack.GetFileLineNumber());
                bStatus = true;
            }
            finally
            {
                log.InfoFormat("Processing of file {0} is complete in task {1} ", cm.InputContractFileName, threadID);
            }

            return bStatus;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cm"></param>
        /// <param name="emailCommon"></param>
        private void processException(int folderCount, ContractManifest cm, EmailCommon emailCommon)
        {
            string destFilePath = string.Format("{0}\\{1}", cm.ExceptionsContractFolder, Path.GetFileName(cm.InputContractFileName));
            if (File.Exists(destFilePath))
            {
                log.WarnFormat("Consumer file exception, file already exists. Deleting |{0}| and moving the new file |{1}|.", destFilePath, cm.InputContractFileName);
                File.Delete(destFilePath);
                File.Move(cm.InputContractFileName, destFilePath);
            }
            else
            {
                File.Move(cm.InputContractFileName, destFilePath);
            }
            log.ErrorFormat("Contract file failed to find consumer's folder, consumer's folder does not exist or more than one match found in search. Folders found {0} using search criteria: LastName: |{1}| FirstName |{2}| DealerCode |{3}|", folderCount, cm.LastName, cm.FirstName, cm.DealerCode);
            String emailSubject = "XYZ Finance Contract issue - Consumer folder missing";
            String emailBody = String.Format("Consumer folder could not be found or multiple folders found. Scanned file: {0} moved to {1}. Once the issue is resolved copy the file to the folder: {2}", cm.InputContractFileName, destFilePath, cm.InputContractFileName);
            if (emailCommon.sendNormalMail(string.Format("{0},{1}", ContractServices.ContractServiceResource.CONFIG_IT_DEPARTMENT_EMAIL_ADDRESS, ContractServices.ContractServiceResource.CONFIG_CUSTOMER_SERVICE_DEPARTMENT_EMAIL_ADDRESS),
                                                                          ContractServices.ContractServiceResource.CONFIG_SERVICE_DEPARTMENT_EMAIL_ADDRESS,
                                                                          ContractServices.ContractServiceResource.CONFIG_SERVICE_DEPARTMENT_EMAIL_ADDRESS_DISPLAY_NAME,
                                                                          null, null, emailSubject, emailBody))
            {
                log.ErrorFormat("Consumer folder email exception failed during emailing. Body contained: {0}", emailBody);
            }

        }

        /// <summary>
        /// This method returns timestamp to milliseconds. NOTE: not unique, in a webfarm or cluster, ID could be duplicated.
        /// </summary>
        /// <returns>String</returns>
        private String GetDayTimestamp()
        {
            return DateTime.Now.ToString("yyyyMMdd");
        }

        #region Insert Text Label

        public void InsertTextToPdf(ContractManifest cm)
        {
            //page lower left [0,0] upper right [612, 792]
            string sLine0 = string.Format("{0}     {1},  {2}     {3}     {4}     {5}  ", cm.LoanNumber, cm.LastName, cm.FirstName, cm.DateRecorded, cm.DealerCode, cm.ACH);
            float offsetHeight = 5;                         // base is 5
            float offsetWidth = 12;                         // base is 12
            float stampHeight = 0;
            float stampWidth = 0;

            try
            {
                using (Stream pdfStream = new FileStream(cm.InputContractFileName, FileMode.Open))
                using (Stream newpdfStream = new FileStream(cm.OutputContractFileName, FileMode.Create, FileAccess.ReadWrite))
                {
                    PdfReader pdfReader = new PdfReader(pdfStream);
                    PdfStamper pdfStamper = new PdfStamper(pdfReader, newpdfStream);

                    PdfImportedPage page = pdfStamper.GetImportedPage(pdfReader, 1);
                    stampHeight = page.Height - offsetHeight;
                    stampWidth = page.Width - offsetWidth;


                    PdfContentByte pdfContentByte = pdfStamper.GetOverContent(1);
                    BaseFont baseFont = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.NOT_EMBEDDED);
                    pdfContentByte.SetColorFill(BaseColor.BLUE);
                    pdfContentByte.SetFontAndSize(baseFont, 9);
                    pdfContentByte.BeginText();
                    //pdfContentByte.ShowTextAligned(PdfContentByte.ALIGN_LEFT, sLine0, 605, 780, 270);
                    pdfContentByte.ShowTextAligned(PdfContentByte.ALIGN_LEFT, sLine0, stampWidth, stampHeight, 270);
                    pdfContentByte.EndText();
                    pdfStamper.Close();
                }
                log.InfoFormat("Metadata label added to consumer file and stored as {0}", cm.OutputContractFileName);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("An error occurred inserting text to the contract. Contract:| {0} |  Error: {1}", formatCMLayout(cm), ex.Message);
            }
        }

        #endregion
        
        #region METADATA

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cm"></param>
        /// <returns></returns>
        public string AddMetadataPDF(ContractManifest cm)
        {
            PdfReader reader = new PdfReader(cm.InputContractFileName);
            Document document;
            PdfWriter writer;

            //open contractManifest.InputContractFileName and add "stamp" to the file.
            //add metadata to the contractManifest.InputContractFileName. - 
            //http://developers.itextpdf.com/examples/stamping-content-existing-pdfs/header-and-footer-examples

            try
            {
                if (reader.AcroForm != null)
                {
                    reader.Close();
                    reader = new PdfReader(FlattenPdfFormToBytes(cm.InputContractFileName));
                }
                iTextSharp.text.Rectangle size = reader.GetPageSizeWithRotation(1);
                document = new Document(size);
                writer = PdfWriter.GetInstance(document, new FileStream(cm.OutputContractFileName, FileMode.Create, FileAccess.Write));
                document.AddTitle("Finance Contract");
                document.AddSubject("Stamped");
                document.AddCreator("XYZ Business Support");
                document.AddAuthor(cm.DealerCode);
                document.AddHeader("Loan Number", cm.LoanNumber);
                document.AddHeader("Name", string.Format("{0} {1}", cm.FirstName, cm.LastName));
                document.AddHeader("Date Recorded", cm.DateRecorded);
                document.AddHeader("Dealer Code", cm.DealerCode);
                document.AddHeader("ACH", cm.ACH);

                document.Open();
                PdfContentByte cb = writer.DirectContent;
                for (int pageNumber = 1; pageNumber <= reader.NumberOfPages; pageNumber++)
                {
                    document.NewPage();
                    PdfImportedPage page = writer.GetImportedPage(reader, pageNumber);
                    cb.AddTemplate(page, 0, 0);
                }
                if (document != null)
                {
                    document.Close();
                    document.Dispose();
                }
                if (writer != null)
                {
                    writer.Close();
                    writer.Dispose();
                }
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
                log.InfoFormat("Metadata added to consumer file and stored as {0}", cm.OutputContractFileName);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("An error occurred adding metadata to the contract. Contract:| {0} |  Error: {1}", formatCMLayout(cm), ex.Message);
            }
            finally
            {
            }
            return cm.OutputContractFileName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private byte[] FlattenPdfFormToBytes(String fileToBeFlattened)
        {
            PdfReader reader = new PdfReader(fileToBeFlattened);
            var memStream = new MemoryStream();
            var stamper = new PdfStamper(reader, memStream) { FormFlattening = true };
            stamper.Close();
            reader.Close();
            return memStream.ToArray();
        }
        #endregion

        #region Misc
        /// <summary>
        /// This method formats the Contract Manifest for logging
        /// </summary>
        /// <param name="cm"></param>
        /// <returns>string</returns>
        private string formatCMLayout(ContractManifest cm)
        {
            return string.Format("Name: {0} {1} InputFile: {2} OutFile: {3} LoanNumber: {4} DealerCode: {5} ACH: {6} DateRecorded: {7}",
                 cm.FirstName, cm.LastName, cm.InputContractFileName, cm.OutputContractFileName, cm.LoanNumber, cm.DealerCode, cm.ACH, cm.DateRecorded);
        }

        #endregion

        #region Search Consumer folders
        /// <summary>
        ///  If the method does not find a folder or finds more than one folder the path returned is empty. If one folder is found, then the folder path is returned.
        /// </summary>
        /// <param name="sSearchPath"></param>
        /// <param name="sLastName"></param>
        /// <param name="sFirstName"></param>
        /// <param name="sDealerCode"></param>
        /// <returns>string[]</returns>
        public string[] FindConsumerInFolder(string sSearchPath, string sLastName, string sFirstName, string sDealerCode)
        {
            string[] listOfCandidates = null;

            try
            {
                if ( !(string.IsNullOrWhiteSpace(sLastName)) && !(string.IsNullOrWhiteSpace(sFirstName)) && !(string.IsNullOrWhiteSpace(sDealerCode)) )
                {
                    string subDirectoryPath = string.Format("{0}\\{1}", sSearchPath, sLastName.Substring(0, 1));        //Retreive first character of lastname
                    if (Directory.Exists(subDirectoryPath))
                    {
                        string searchMask = string.Format("{0}, {1}* *{2}", sLastName, sFirstName, sDealerCode);
                        listOfCandidates = Directory.GetDirectories(subDirectoryPath, searchMask, SearchOption.TopDirectoryOnly);
                    }
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("An error occurred processing the contract. Error: {0}", ex.Message);
            }
            return listOfCandidates;
        }
        #endregion

        #region APP REST resources

        /// <summary>
        /// This method retrieves the required loan information for a specific loan.
        /// Ex.  http://localhost:62541/api/ContractInfo/40836
        /// </summary>
        /// <returns>ContractManifest</returns>
        public ContractManifest getLoanInfo(RESTManifest rm, string sLoanNumber)
        {
            ContractManifest cm = new ContractManifest();
            LoanInfoView liv = new LoanInfoView();

            try
            {
                var client = new RestSharp.RestClient(string.Format("http://{0}", rm.baseUri));
                var request = new RestRequest();
                request.Method = Method.GET;
                request.Resource = string.Format("/{0}/{1}", rm.uriResource, sLoanNumber);

                IRestResponse restResponse = client.Execute(request);
                if (restResponse.ErrorException != null)
                {
                    const string message = "Error retrieving response.  Check inner details for more info.";
                    log.ErrorFormat("{0} Exception: {1} {2}", message, restResponse.ErrorException, restResponse.ErrorMessage);
                }
                else
                {
                    liv = JsonConvert.DeserializeObject<LoanInfoView>(restResponse.Content);
                    cm.FirstName = liv.FirstName1;
                    cm.LastName = liv.LastName1;
                    cm.LoanNumber = liv.Loan_Number;
                    cm.ACH = liv.ACH;
                    cm.DateRecorded = liv.Contract_Date;
                    cm.DealerCode = liv.Dealer_Code;
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("An error occurred processing the web service call to get contract information. Exception: {0}", ex.Message);
            }
            return cm;
        }

        #endregion
        
        #region APP IMPORT WS
        /// <summary>
        /// 
        //  @"http://XYZwebsvc01/appwebservice/service.asmx/ImportXMLUsingDBAuthentication2";
        //  @"http://testsql/APP%20Web%20Service/Service.asmx?ImportXMLUsingDBAuthentication2";
        /// </summary>
        /// <param name="nm"></param>
        /// <param name="sLoanNumber"></param>
        private void UpdateAPP(APPWebServiceManifest nm, string sLoanNumber)
        {
            string sUrl = string.Empty;
            string wsResponse = string.Empty;
            XmlDocument xmlRequestDoc = null;

            sUrl = ContractServices.ContractServiceResource.APPRestImportUrl;

            try
            {
                xmlRequestDoc = FormatXmlLoanUpdate(sLoanNumber);
                nm.APPImportString = xmlRequestDoc.OuterXml;

                string[] pKeys = new string[7];
                pKeys[0] = "ServerName";
                pKeys[1] = "DatabaseName";
                pKeys[2] = "db_username";
                pKeys[3] = "db_password";
                pKeys[4] = "app_username";
                pKeys[5] = "app_password";
                pKeys[6] = "ImportString";

                string[] pValues = new string[7];
                pValues[0] = nm.DBServer;
                pValues[1] = nm.Database;
                //            pValues[2] = "MSSQL";
                pValues[2] = nm.APPDBUsername;
                pValues[3] = nm.APPDBPassword;
                pValues[4] = nm.APPUsername;
                pValues[5] = nm.APPPassword;
                pValues[6] = nm.APPImportString;

                wsResponse = HttpPost(sUrl, pKeys, pValues);
                if (wsResponse.Contains("Unable to connect to the database"))
                {
                    log.ErrorFormat("Update APP failed. Response: {0}", wsResponse);
                }
                else if (wsResponse.Contains("<string xmlns=\"http://nortridge.com/\" />"))   //This means success to APP
                {
                    log.InfoFormat("Updated APP success. Response: {0}", wsResponse);
                }
                else
                {
                    log.InfoFormat("Updated APP failed. Response: {0}", wsResponse);
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Failed to update APP. Exception: {0}", ex.Message);
            }
        }



        /// <summary>
        ///     <APP CommitBlock = "0" EnforceTagExistence="1"> 
        ///     <LOAN UpdateFlag = "1" LoanNumber="1160001" >
        ///      <LOANDETAIL2 UserDefined19 = "1" />
        ///      < LOANCOMMENTS Comment="Contract Scanned" CommentDescription="Contract Original Scanned to consumers folder" />
        ///      </LOAN>
        ///      </APP>"
        /// </summary>
        /// <param name="sLoanNumber"></param>
        /// <returns>XmlDocument</returns>
        private XmlDocument FormatXmlLoanUpdate(string sLoanNumber)
        {
            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc = CreateXMLImportDoc();
            XmlElement docRoot = CreateXMLImportRoot(xmlDoc, true, true);
            XmlElement eleLoan = xmlDoc.CreateElement("LOAN");
            XmlElement eleLoanDetail2 = xmlDoc.CreateElement("LOANDETAIL2");
            XmlElement eleLoanComments = xmlDoc.CreateElement("LOANCOMMENTS");
            eleLoan.SetAttribute("UpdateFlag", "1");
            eleLoan.SetAttribute("LoanNumber", sLoanNumber);
            eleLoanDetail2.SetAttribute("UserDefined19",  "1");
            eleLoanComments.SetAttribute("Comment", "Contract Scanned");
            eleLoanComments.SetAttribute("CommentDescription", "Contract Original Scanned to consumers folder");
            docRoot.AppendChild(eleLoan);
            eleLoan.AppendChild(eleLoanDetail2);
            eleLoan.AppendChild(eleLoanComments);
            log.DebugFormat("APP Import - updating loan {0} with {1}", sLoanNumber, xmlDoc.OuterXml);

            return xmlDoc;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>XmlDocument</returns>
        private XmlDocument CreateXMLImportDoc()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);

            return xmlDoc;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="commitBlock"></param>
        /// <param name="enforceTags"></param>
        /// <returns>XmlElement</returns>
        private XmlElement CreateXMLImportRoot(XmlDocument xmlDoc, bool commitBlock, bool enforceTags)
        {
            XmlElement rootEle = xmlDoc.CreateElement("APP");
            rootEle.SetAttribute("CommitBlock", (commitBlock ? "1" : "0"));
            rootEle.SetAttribute("EnforceTagExistence", (enforceTags ? "1" : "0"));
            xmlDoc.AppendChild(rootEle);

            return rootEle;
        }

        #endregion

        #region REST POST

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="paramName"></param>
        /// <param name="paramVal"></param>
        /// <returns>string</returns>
        private string HttpPost(string url, string[] paramName, string[] paramVal)
        {
            string wsResult = string.Empty;
            HttpWebRequest req = WebRequest.Create(new Uri(url)) as HttpWebRequest;
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";

            try
            {
                // Build a string with all the params, properly encoded.
                // We assume that the arrays paramName and paramVal are
                // of equal length:
                StringBuilder sbParameters = new StringBuilder();
                for (int i = 0; i < paramName.Length; i++)
                {
                    sbParameters.Append(paramName[i]);
                    sbParameters.Append("=");
                    sbParameters.Append(HttpUtility.UrlEncode(paramVal[i]));
                    sbParameters.Append("&");
                }

                // Encode the parameters as form data:
                byte[] formData =
                    UTF8Encoding.UTF8.GetBytes(sbParameters.ToString());
                req.ContentLength = formData.Length;

                // Send the request:
                using (Stream post = req.GetRequestStream())
                {
                    post.Write(formData, 0, formData.Length);
                }

                // Pick up the response:
                using (HttpWebResponse resp = req.GetResponse()  as HttpWebResponse)
                {
                    StreamReader reader =
                        new StreamReader(resp.GetResponseStream());
                    wsResult = reader.ReadToEnd();
                }
                log.InfoFormat("SUCCESS: APP Import updated loan.");
            }
            catch (Exception ex)
            {
                log.ErrorFormat("FAILED: APP Import REST call returned an exception: {0}", ex.Message);
             }   

             return wsResult;
        }

        #endregion

        #region WATERMARK

        /// <summary>
        ///  Add a watermark to the contract file.
        /// Ex. http://developers.itextpdf.com/examples/stamping-content-existing-pdfs/watermark-examples
        /// </summary>
        /// <param name="contractManifest"></param>
        /// <returns>string</returns>
        public string WatermarkPDF(ContractManifest contractManifest, string sWatermark)
        {
            PdfReader reader = new PdfReader(contractManifest.InputContractFileName);
            PdfStamper pdfStamper;

            try
            {
                if (reader.AcroForm != null)
                {
                    reader.Close();
                    reader = new PdfReader(FlattenPdfFormToBytes(contractManifest.InputContractFileName));
                }
                pdfStamper = new PdfStamper(reader, new FileStream(contractManifest.OutputContractFileName, FileMode.Create));

                for (int pageIndex = 1; pageIndex <= reader.NumberOfPages; pageIndex++)
                {
                    //Rectangle class in iText represent geomatric representation... in this case, rectanle object would contain page geomatry
                    iTextSharp.text.Rectangle pageRectangle = reader.GetPageSizeWithRotation(pageIndex);
                    //pdfcontentbyte object contains graphics and text content of page returned by pdfstamper
                    PdfContentByte pdfData = pdfStamper.GetUnderContent(pageIndex);
                    //create fontsize for watermark
                    pdfData.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED), 40);
                    //create new graphics state and assign opacity
                    PdfGState graphicsState = new PdfGState();
                    graphicsState.FillOpacity = 0.4F;
                    //set graphics state to pdfcontentbyte
                    pdfData.SetGState(graphicsState);
                    //set color of watermark
                    pdfData.SetColorFill(BaseColor.BLUE);
                    //indicates start of writing of text
                    pdfData.BeginText();
                    //show text as per position and rotation
                    //                    pdfData.ShowTextAligned(Element.ALIGN_CENTER, sWatermark, pageRectangle.Width / 2, pageRectangle.Height / 2, 45);
                    pdfData.ShowTextAligned(Element.ALIGN_TOP, sWatermark, 0, 0, 0);
                    //call endText to invalid font set
                    pdfData.EndText();
                }
                pdfStamper.Close();
                reader.Close();
            }
            catch (Exception ex)
            {
                log.ErrorFormat("An error occurred processing the contract. Error: {0}", ex.Message);
            }
            finally
            {
            }

            return contractManifest.OutputContractFileName;
        }


        /// <summary>
        /// Method that will utilize iTextSharp to write the <see cref="stringToWriteToPdf"/> to the 
        /// pdf on each page of the PDF.
        /// </summary>
        /// <param name="sourceFile">The PDf File</param>
        /// <param name="stringToWriteToPdf">The text to write to the pdf</param>
        /// <returns>byte[]</returns>
        public byte[] WriteToPdf(FileInfo sourceFile, string stringToWriteToPdf)
        {
            PdfReader reader = new PdfReader(sourceFile.FullName);

            using (MemoryStream memoryStream = new MemoryStream())
            {
                //
                // PDFStamper is the class we use from iTextSharp to alter an existing PDF.
                //
                PdfStamper pdfStamper = new PdfStamper(reader, memoryStream);

                for (int i = 1; i <= reader.NumberOfPages; i++) // Must start at 1 because 0 is not an actual page.
                {
                    //
                    // If you ask for the page size with the method getPageSize(), you always get a
                    // Rectangle object without rotation (rot. 0 degrees)—in other words, the paper size
                    // without orientation. That’s fine if that’s what you’re expecting; but if you reuse
                    // the page, you need to know its orientation. You can ask for it separately with
                    // getPageRotation(), or you can use getPageSizeWithRotation(). - (Manning Java iText Book)
                    //   
                    //
                    iTextSharp.text.Rectangle pageSize = reader.GetPageSizeWithRotation(i);

                    //
                    // Gets the content ABOVE the PDF, Another option is GetUnderContent(...)  
                    // which will place the text below the PDF content. 
                    //
                    PdfContentByte pdfPageContents = pdfStamper.GetUnderContent(i);
                    pdfPageContents.BeginText(); // Start working with text.

                    //
                    // Create a font to work with 
                    //
                    BaseFont baseFont = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, Encoding.ASCII.EncodingName, false);
                    pdfPageContents.SetFontAndSize(baseFont, 40); // 40 point font
                    pdfPageContents.SetRGBColorFill(255, 0, 0); // Sets the color of the font, RED in this instance

                    //
                    // Angle of the text. This will give us the angle so we can angle the text diagonally 
                    // from the bottom left corner to the top right corner through the use of simple trigonometry. 
                    //
                    float textAngle = (float)GetHypotenuseAngleInDegreesFrom(pageSize.Height, pageSize.Width);

                    //
                    // Note: The x,y of the Pdf Matrix is from bottom left corner. 
                    // This command tells iTextSharp to write the text at a certain location with a certain angle.
                    // Again, this will angle the text from bottom left corner to top right corner and it will 
                    // place the text in the middle of the page. 
                    //
                    pdfPageContents.ShowTextAligned(PdfContentByte.ALIGN_CENTER, stringToWriteToPdf,
                                                    pageSize.Width / 2,
                                                    pageSize.Height / 2,
                                                    textAngle);

                    pdfPageContents.EndText(); // Done working with text
                }
                pdfStamper.FormFlattening = true; // enable this if you want the PDF flattened. 
                pdfStamper.Close(); // Always close the stamper or you'll have a 0 byte stream. 

                return memoryStream.ToArray();
            }
        }



        /// <summary>
        ///  This method calculates the 45 degree angle to put the watermark
        //http://www.regentsprep.org/Regents/Math/rtritrig/LtrigA.htm
        // Tan <angle> = opposite/adjacent
        // Math.Atan2: http://msdn.microsoft.com/en-us/library/system.math.atan2(VS.80).aspx 
        /// </summary>
        /// <param name="opposite"></param>
        /// <param name="adjacent"></param>
        /// <returns>double</returns>
        private double GetHypotenuseAngleInDegreesFrom(double opposite, double adjacent)
        {

            double radians = Math.Atan2(opposite, adjacent); // Get Radians for Atan2
            double angle = radians * (180 / Math.PI); // Change back to degrees
            return angle;
        }
        #endregion

        #region TESTING
        public string TESTWatermarkPDF(ContractManifest contractManifest, string sWatermark)
        {
            string watermarkedFile = @"C:\Projects\Workspaces\LoanContractManagement\Work\Stamped\WatermarkedFILE.pdf";
            // Creating watermark on a separate layer
            // Creating iTextSharp.text.pdf.PdfReader object to read the Existing PDF Document
            PdfReader reader1 = new PdfReader(contractManifest.InputContractFileName);
            using (FileStream fs = new FileStream(watermarkedFile, FileMode.Create, FileAccess.Write, FileShare.None))
            // Creating iTextSharp.text.pdf.PdfStamper object to write Data from iTextSharp.text.pdf.PdfReader object to FileStream object
            using (PdfStamper stamper = new PdfStamper(reader1, fs))
            {
                // Getting total number of pages of the Existing Document
                int pageCount = reader1.NumberOfPages;

                // Create New Layer for Watermark
                PdfLayer layer = new PdfLayer("WatermarkLayer", stamper.Writer);
                // Loop through each Page
                for (int i = 1; i <= pageCount; i++)
                {
                    // Getting the Page Size
                    iTextSharp.text.Rectangle rect = reader1.GetPageSize(i);

                    // Get the ContentByte object
                    PdfContentByte cb = stamper.GetUnderContent(i);

                    // Tell the cb that the next commands should be "bound" to this new layer
                    cb.BeginLayer(layer);
                    cb.SetFontAndSize(BaseFont.CreateFont(
                      BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED), 50);

                    PdfGState gState = new PdfGState();
                    gState.FillOpacity = 0.25f;
                    cb.SetGState(gState);

                    cb.SetColorFill(BaseColor.BLACK);
                    cb.BeginText();
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, sWatermark, rect.Width / 2, rect.Height / 2, 45f);
                    cb.EndText();

                    // Close the layer
                    cb.EndLayer();
                }
            }
            return contractManifest.OutputContractFileName;
        }

        #endregion


    }
}
