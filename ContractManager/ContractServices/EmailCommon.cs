﻿using System;
using System.Net.Mail;
using log4net;

namespace XYZContractServices
{
    /// <summary>
    /// This class contains the XYZ email settings.
    /// </summary>
    public class SMTPSettings
    {
        public String Server { get; set; }
        public String Port { get; set; }
        public String User { get; set; }
        public String Password { get; set; }
    }

    /// <summary>
    /// This class contains email message content fields.
    /// </summary>
    public class SMTPMEssage
    {
        public MailPriority MailPriority;
        public String To;
        public String From;
        public String FromDisplayName;
        public String CC;
        public String BCC;
        public String Subject;
        public String Body;
    }

    /// <summary>
    /// This class contains the XYZ email functionality.
    /// </summary>
    public class EmailCommon
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(EmailCommon));

        #region EMail methods
        /// <summary>
        /// This is for sending email with a express priority.
        /// </summary>
        public Boolean sendExpressMail(string to, string from, string fromDisplayName, string cc, string bcc, string subject, string body)
        {
            Boolean status = false;
            SMTPMEssage smtpMsg = new SMTPMEssage();
            smtpMsg.MailPriority = MailPriority.High;
            smtpMsg.To = to;
            smtpMsg.From = from;
            smtpMsg.FromDisplayName = fromDisplayName;
            smtpMsg.CC = cc;
            smtpMsg.BCC = bcc;
            smtpMsg.Subject = subject;
            smtpMsg.Body = body;
            status = sendMail(smtpMsg);
            return status;
        }

        /// <summary>
        /// This is for sending email with a slow priority.
        /// </summary>
        public Boolean sendSlowMail(string to, string from, string fromDisplayName, string cc, string bcc, string subject, string body)
        {
            Boolean status = false;
            SMTPMEssage smtpMsg = new SMTPMEssage();
            smtpMsg.MailPriority = MailPriority.Low;
            smtpMsg.To = to;
            smtpMsg.From = from;
            smtpMsg.FromDisplayName = fromDisplayName;
            smtpMsg.CC = cc;
            smtpMsg.BCC = bcc;
            smtpMsg.Subject = subject;
            smtpMsg.Body = body;
            status = sendMail(smtpMsg);
            return status;
        }

        /// <summary>
        /// This is for sending email with a normal priority.
        /// </summary>
        public Boolean sendNormalMail(string to, string from, string fromDisplayName, string cc, string bcc, string subject, string body)
        {
            Boolean status = false;
            SMTPMEssage smtpMsg = new SMTPMEssage();
            smtpMsg.MailPriority = MailPriority.Normal;
            smtpMsg.To = to;
            smtpMsg.From = from;
            smtpMsg.FromDisplayName = fromDisplayName;
            smtpMsg.CC = cc;
            smtpMsg.BCC = bcc;
            smtpMsg.Subject = subject;
            smtpMsg.Body = body;
            status = sendMail(smtpMsg);
            return status;
        }

        /// <summary>
        /// This is for sending email to the IT Department with an express priority.
        /// </summary>
        /// <param name="sFromEmailAddress"></param>
        /// <param name="sFromEmailAddressDisplay"></param>
        /// <param name="sToEmailAddres"></param>
        /// <param name="sSubject"></param>
        /// <param name="sBody"></param>
        /// <returns></returns>
        public Boolean sendITDepartmentAlert(String sFromEmailAddress, String sFromEmailAddressDisplay, String sToEmailAddres, String sSubject, String sBody)
        {
            Boolean status = false;
            status = sendExpressMail(sToEmailAddres, sFromEmailAddress, sFromEmailAddressDisplay, null, null, sSubject, sBody);
            return status;
        }

        private void LoadSMTPSettings(ref SMTPSettings primary, ref SMTPSettings secondary)
        {
            //TODO     CONNECT THIS TO THE STORED PROCEDURE IN NON_APPDATA
            //            XYZFinancialCommon.DataCommon dataCommon = new XYZFinancialCommon.DataCommon();

            //primary.Server = dataCommon.GetConfigurationKeyValue(CommonConstants.CONFIG_PRIMARY_SMTP_SERVER);
            //primary.Port = dataCommon.GetConfigurationKeyValue(CommonConstants.CONFIG_PRIMARY_SMTP_PORT);
            //primary.User = dataCommon.GetConfigurationKeyValue(CommonConstants.CONFIG_PRIMARY_SMTP_USER);
            //primary.Password = dataCommon.GetConfigurationKeyValue(CommonConstants.CONFIG_PRIMARY_SMTP_PASSWORD);

            //secondary.Server = dataCommon.GetConfigurationKeyValue(CommonConstants.CONFIG_SECONDARY_SMTP_SERVER);
            //secondary.Port = dataCommon.GetConfigurationKeyValue(CommonConstants.CONFIG_SECONDARY_SMTP_PORT);
            //secondary.User = dataCommon.GetConfigurationKeyValue(CommonConstants.CONFIG_SECONDARY_SMTP_USER);
            //secondary.Password = dataCommon.GetConfigurationKeyValue(CommonConstants.CONFIG_SECONDARY_SMTP_PASSWORD);

            primary.Server = ContractServices.ContractServiceResource.CONFIG_PRIMARY_SMTP_SERVER;
            primary.Port = ContractServices.ContractServiceResource.CONFIG_PRIMARY_SMTP_PORT;
            primary.User = ContractServices.ContractServiceResource.CONFIG_PRIMARY_SMTP_USER;
            primary.Password = ContractServices.ContractServiceResource.CONFIG_PRIMARY_SMTP_PASSWORD;

            secondary.Server = ContractServices.ContractServiceResource.CONFIG_SECONDARY_SMTP_SERVER;
            secondary.Port = ContractServices.ContractServiceResource.CONFIG_SECONDARY_SMTP_PORT;
            secondary.User = ContractServices.ContractServiceResource.CONFIG_SECONDARY_SMTP_USER;
            secondary.Password = ContractServices.ContractServiceResource.CONFIG_SECONDARY_SMTP_PASSWORD;
        }


        /// <summary>
        /// This method is for sending email.
        /// </summary>
        /// <param name="smtpMessage"></param>
        /// <returns>Boolean</returns>
        private Boolean sendMail(SMTPMEssage smtpMessage)
        {
            Boolean status = false;
            String[] multiTO = null;
            String[] multiCC = null;
            String[] multiBCC = null;
            SMTPSettings smtpPrimary = new SMTPSettings();
            SMTPSettings smtpSecondary = new SMTPSettings();

            LoadSMTPSettings(ref smtpPrimary, ref smtpSecondary);

            if (string.IsNullOrEmpty(smtpMessage.From))
            { log.Error("The FROM address is empty/null"); }
            if (String.IsNullOrEmpty(smtpMessage.To))
            { log.Error("The TO address is empty/null"); }

            MailMessage objMessage = new MailMessage();
            objMessage.Priority = smtpMessage.MailPriority;
            objMessage.ReplyToList.Add(smtpMessage.To);
            if (!String.IsNullOrEmpty(smtpMessage.FromDisplayName))
            {
                objMessage.From = new MailAddress(smtpMessage.From, smtpMessage.FromDisplayName);
            }
            else
            {
                objMessage.From = new MailAddress(smtpMessage.From);
            }

            if (!String.IsNullOrWhiteSpace(smtpMessage.To))
            {
                multiTO = smtpMessage.To.Split(',');
                foreach (String sTo in multiTO)             // handle multipe TOs
                {
                    objMessage.To.Add(new MailAddress(sTo));
                }
            }

            if (!String.IsNullOrWhiteSpace(smtpMessage.CC))
            {
                multiCC = smtpMessage.CC.Split(',');
                foreach (String sCC in multiCC)         // handle multiple CCs
                {
                    objMessage.CC.Add(new MailAddress(sCC));
                }
            }
            if (!String.IsNullOrEmpty(smtpMessage.BCC))
            {
                multiBCC = smtpMessage.BCC.Split(',');
                foreach (String sBCC in multiBCC)       // handle multiple BCCs
                {
                    objMessage.Bcc.Add(new MailAddress(sBCC));
                }
            }
            objMessage.Subject = smtpMessage.Subject;
            //TODO mtr can a body be html??
            objMessage.Body = smtpMessage.Body;

            //Primary SMTP Settings
            SmtpClient SMTPServerPrimary = new SmtpClient(smtpPrimary.Server, Convert.ToInt32(smtpPrimary.Port));
            SMTPServerPrimary.UseDefaultCredentials = false;
            SMTPServerPrimary.EnableSsl = true;
            SMTPServerPrimary.Credentials = new System.Net.NetworkCredential(smtpPrimary.User, smtpPrimary.Password);

            //Send mail
            try
            {
                SMTPServerPrimary.Send(objMessage);
                log.Info(String.Format("Email sent via {0} from {1} to {2} with subject {3} for message of {4}", smtpPrimary.Server, objMessage.From, objMessage.To, objMessage.Subject, objMessage.Body));
            }
            catch (SmtpException primarySMTPException)
            {
                log.Error(String.Format("Primary SMTP Server {0} is having difficulty.", smtpPrimary.Server), primarySMTPException);
                log.Error(String.Format("Attempting to send message: From: |{0}| To: |{1}| CC: |{2}| BCC: |{3}| Subject: |{4}|", objMessage.From, objMessage.To, objMessage.CC, objMessage.Bcc, objMessage.Subject));
                try
                {
                    //SMTP server failed to connection for some reason, attempt secondary email server
                    SmtpClient SecondarySMTPServer = new SmtpClient(smtpSecondary.Server, Convert.ToInt32(smtpSecondary.Port));
                    SecondarySMTPServer.UseDefaultCredentials = false;
                    SecondarySMTPServer.EnableSsl = true;
                    SecondarySMTPServer.Credentials = new System.Net.NetworkCredential(smtpSecondary.User, smtpSecondary.Password);
                    SecondarySMTPServer.Send(objMessage);
                    log.Info(String.Format("Email sent via {0} from {1} to {2} with subject {3} for message of {4}", smtpSecondary.Server, objMessage.From, objMessage.To, objMessage.Subject, objMessage.Body));
                }
                catch (Exception secondarySMTPException)
                {
                    status = true;
                    log.Error(String.Format("Secondary SMTP Server {0} is having difficulty.", smtpSecondary.Server), secondarySMTPException);
                    log.Error(String.Format("Attempting to send message: From: |{0}| To: |{1}| CC: |{2}| BCC: |{3}| Subject: |{4}|", objMessage.From, objMessage.To, objMessage.CC, objMessage.Bcc, objMessage.Subject));
                }
            }
            catch (Exception primaryException)
            {
                status = true;
                log.Error(primaryException);
            }
            return status;
        }
        #endregion

    }
}
