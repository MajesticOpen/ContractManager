﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XYZContractServices
{
    public interface IContracts
    {
        string AddMetadataPDF(ContractManifest contractManifest);
        string WatermarkPDF(ContractManifest contractManifest, string sWatermark);
        string[] FindConsumerInFolder(string sSearchPath, string sLastName, string sFirstName, string sDealerCode);
        Boolean processFinanceContract(RESTManifest rm, ContractManifest cm, APPWebServiceManifest nm, EmailCommon emailCommon);
    }
}
