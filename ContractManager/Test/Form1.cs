﻿using System;
using System.IO;
using System.Windows.Forms;
using XYZContractServices;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Xml;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Test
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            tbIntTestScanFile.Text = @"C:\Projects\Workspaces\LoanContractManagement\Work\ARMSTRONG, JASON & HEADLEY, JASON FQ117 TexasRetailContract.pdf";
            LoadScannedFileList();
        }

        private void LoadScannedFileList()
        {
            cbIntProductionScanFile.Items.Clear();
            cbIntProductionScanFile.Items.AddRange(Directory.GetFiles(@"C:\scratch\XYZFILE01\Scans\Contracts2Process"));
            if (cbIntProductionScanFile.Items.Count > 0)
            {
                cbIntProductionScanFile.SelectedIndex = 0;
            }
            cbIntProductionScanFile.Refresh();
            Refresh();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Contracts cs = new Contracts();
            ContractManifest cm = new ContractManifest();
            RESTManifest rm = new RESTManifest();

            //http://localhost:62541/api/xyz_GetCifInfo/40836
//            rm.baseUri = "localhost:62541";
            rm.baseUri = "xyzweb01:8080";
            rm.uriResource = "api/ContractInfo";

            //            cm = contracts.getManifest(textBox1.Text);
            string stampedFile = string.Empty;
            string sLoanNumber = string.Empty;

            if (File.Exists(cm.InputContractFileName))
            {
                sLoanNumber = Path.GetFileName(tbIntTestScanFile.Text);
                cm = cs.getLoanInfo(rm, sLoanNumber);
                stampedFile = cs.AddMetadataPDF(cm);
                File.Delete(cm.InputContractFileName);
//                eventLog.WriteEntry(string.Format("File processed and moved from {0} to {1}", cm.InputContractFileName, cm.OutputContractFileName), EventLogEntryType.Information, eventId++);
                label1.Text = string.Format("File {0} - COMPLETED.", stampedFile);
            }
            else
            {
                label1.Text = string.Format("File {0} - does NOT exist.", stampedFile);
//                eventLog.WriteEntry(string.Format("File to be processed was not found -  {0}", cm.OutputContractFileName), EventLogEntryType.Information, eventId++);
            }




        }

        private void button2_Click(object sender, EventArgs e)
        {
            Contracts contracts = new Contracts();
            ContractManifest cm = new ContractManifest();
//TODO FIXTHIS            cm = contracts..getManifest(tbIntTestScanFile.Text);
            string watermarkFile = string.Empty;
            FileInfo fi = new FileInfo(tbIntTestScanFile.Text);

            if (File.Exists(cm.InputContractFileName))
            {
                string sOutfile = contracts.TESTWatermarkPDF(cm, "PAID IN FULL");
                watermarkFile = contracts.WatermarkPDF(cm, "PAID IN FULL");
                byte[] wFileBytes = contracts.WriteToPdf(fi, "PAID IN FULL");

                using (Stream file = File.OpenWrite(@"C:\Projects\Workspaces\LoanContractManagement\Work\Stamped\Watermark.pdf"))
                {
                    file.Write(wFileBytes, 0, wFileBytes.Length);
                }

                File.Delete(cm.InputContractFileName);
                //                eventLog.WriteEntry(string.Format("File processed and moved from {0} to {1}", cm.InputContractFileName, cm.OutputContractFileName), EventLogEntryType.Information, eventId++);
                label1.Text = string.Format("File {0} - COMPLETED.", watermarkFile);
            }
            else
            {
                label1.Text = string.Format("File {0} - does NOT exist.", watermarkFile);
                //                eventLog.WriteEntry(string.Format("File to be processed was not found -  {0}", cm.OutputContractFileName), EventLogEntryType.Information, eventId++);
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Contracts contracts = new Contracts();
            ContractManifest cm = new ContractManifest();
//TODO FIXTHIS            cm = contracts.getManifest(tbIntTestScanFile.Text);

            if (File.Exists(cm.InputContractFileName))
            {
                contracts.InsertTextToPdf(cm);

                File.Delete(cm.InputContractFileName);
                //                eventLog.WriteEntry(string.Format("File processed and moved from {0} to {1}", cm.InputContractFileName, cm.OutputContractFileName), EventLogEntryType.Information, eventId++);
                label1.Text = string.Format("File {0} - COMPLETED.", cm.OutputContractFileName);
            }
            else
            {
                label1.Text = string.Format("File {0} - does NOT exist.", cm.OutputContractFileName);
                //                eventLog.WriteEntry(string.Format("File to be processed was not found -  {0}", cm.OutputContractFileName), EventLogEntryType.Information, eventId++);
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            EmailCommon emailCommon = new EmailCommon();
            Contracts cs = new Contracts();
            RESTManifest rm = new RESTManifest();
            ContractManifest cm = new ContractManifest();
            APPWebServiceManifest nm = new APPWebServiceManifest();
            cm.OutputContractFileName = @"C:\scratch\XYZFILE01\Consumer Files";

            rm.baseUri = "xyzweb01:8080";
 //           rm.baseUri = "localhost:62541";
            rm.uriResource = "api/ContractInfo";

            cm.InputContractFileName = tbIntTestScanFile.Text;
             cs.processFinanceContract(rm, cm, nm, emailCommon);
            
        }

        private void buttonTest1_Click(object sender, EventArgs e)
        {
            Contracts cs = new Contracts();
            //BABB, CHELSEA & DALTON RC265
             string[] sResult = cs.FindConsumerInFolder(tbSearchFolder.Text, "BABB", "CHELSEA", "RC265");
            if (sResult.Length == 0)
            {
                tbResult.Text = "No results found.";
            }
            else
            {
                tbResult.Text = string.Join(Environment.NewLine, sResult);
            }
        }

        private void buttonTest2_Click(object sender, EventArgs e)
        {
            Contracts cs = new Contracts();
            //BABB, ROBERT & BABB, CAROLYN RC133
            string[] sResult = cs.FindConsumerInFolder(tbSearchFolder.Text, "BABB", "ROBERT", "RC133");
            if (sResult.Length == 0)
            {
                tbResult.Text = "No results found.";
            }
            else
            {
                tbResult.Text = string.Join(Environment.NewLine, sResult);
            }
        }

        private void buttonTest3_Click(object sender, EventArgs e)
        {
            Contracts cs = new Contracts();
            //BABCOCK, JENNIFER & JOHNSON, WARREN FQ117
            string[] sResult = cs.FindConsumerInFolder(tbSearchFolder.Text, "BABCOCK", "JENNIFER", "FQ117");
            if (sResult.Length == 0)
            {
                tbResult.Text = "No results found.";
            }
            else
            {
                tbResult.Text = string.Join(Environment.NewLine, sResult);
            }
        }

        private void buttonTest4_Click(object sender, EventArgs e)
        {
            Contracts cs = new Contracts();
            //BABERS, ROSE FQ130
            string[] sResult = cs.FindConsumerInFolder(tbSearchFolder.Text, "BABERS", "ROSE", "FQ130");
            if (sResult.Length == 0)
            {
                tbResult.Text = "No results found.";
            }
            else
            {
                tbResult.Text = string.Join(Environment.NewLine, sResult);
            }
        }

        private void buttonTest5_Click(object sender, EventArgs e)
        {
            Contracts cs = new Contracts();
            //        BACANI, JEAN VICTORIA SM189
            string[] sResult = cs.FindConsumerInFolder(tbSearchFolder.Text, "BACANI", "JEAN VICTORIA", "SM189");
            if (sResult.Length == 0)
            {
                tbResult.Text = "No results found.";
            }
            else
            {
                tbResult.Text = string.Join(Environment.NewLine, sResult);
            }
        }

        private void buttonTest6_Click(object sender, EventArgs e)
        {
            Contracts cs = new Contracts();
            //        BACANI, JEAN-VICTORIA SM189
            string[] sResult = cs.FindConsumerInFolder(tbSearchFolder.Text, "BACANI", "JEAN-VICTORIA", "SM189");
            if (sResult.Length == 0)
            {
                tbResult.Text = "No results found.";
            }
            else
            {
                tbResult.Text = string.Join(Environment.NewLine, sResult);
            }
        }

        private void buttonREST_Click(object sender, EventArgs e)
        {
            Contracts cs = new Contracts();
            ContractManifest cm = new ContractManifest();
            RESTManifest rm = new RESTManifest();

            rm.baseUri = "xyzweb01:8080";
//            rm.baseUri = "localhost:62541";
            rm.uriResource = "api/ContractInfo";

            cm = cs.getLoanInfo(rm, tbLoanNumber.Text);
            tbRESTResult.Text = cm.ToString();
        }

        //SINGLE PROCESSING OF REQUEST - CONTRACT
        private void buttonProdStampMetaData_Click(object sender, EventArgs e)
        {
            Contracts cs = new Contracts();
            RESTManifest rm = new RESTManifest();
            ContractManifest cm = new ContractManifest();
            APPWebServiceManifest nm = new APPWebServiceManifest();
            EmailCommon emailCommon = new EmailCommon();
            cm.OutputContractFileName = @"C:\scratch\XYZFILE01\Consumer Files";

            rm.baseUri = "xyzweb01:8080";
//            rm.baseUri = "localhost:62541";
            rm.uriResource = "api/ContractInfo";

//            nm.DBServer = "XYZSQL";
            nm.DBServer = "TESTSQL";
            nm.Database = "XYZ Financial";
            nm.APPDBUsername = "sa";
//            nm.APPDBPassword = "Finance2345";
            nm.APPDBPassword = "13Qeadzc";
            nm.APPUsername = "admin";
            nm.APPPassword = "Finance1234";

            cm.InputContractFileName = cbIntProductionScanFile.SelectedItem.ToString();
            if (cs.processFinanceContract(rm, cm, nm, emailCommon))
            {
                lblProductionTestFileStatus.Text = "Process failed.";
            }
            else
            {
                lblProductionTestFileStatus.Text = "Process passed.";
            }
            LoadScannedFileList();

        }

        private void btnGOThreads_Click(object sender, EventArgs e)
        {

            //output:
            //   Outer task beginning
            //   Outer task Completed
            //  detached task completed

            var parent = Task.Factory.StartNew(() => {
                Console.WriteLine("Parent task beginning.");
                for (int cnt = 0; cnt < 10; cnt++)
                {
                                        var child = Task.Factory.StartNew(() =>  { processFinancialContract(cnt); } );
                    //Console.WriteLine(string.Format("Detached task {0} started with wait time of {1} and thread id of {2}.", cnt, waitTime, threadID));
                    //Thread.SpinWait(waitTime);
                    //Console.WriteLine(string.Format("Detached task {0} completed.", cnt));
                }
            });

            parent.Wait();
            Console.WriteLine("Parent task completed.");
        }

        private void processFinancialContract(object cnt)
        {
            Random rnd = new Random();

            int waitTime = rnd.Next(3000000, 7000000);
            int threadID = Thread.CurrentThread.ManagedThreadId;
            Console.WriteLine(string.Format("Detached task {0} started with wait time of {1} and thread id of {2}.", cnt, waitTime, threadID));
            Thread.SpinWait(waitTime);
            Console.WriteLine(string.Format("Detached task {0} completed.", cnt));
        }

        private void OPTION1buttonProdStampMetaDataALL_Click(object sender, EventArgs e)
        {
            EmailCommon emailCommon = new EmailCommon();
            Contracts cs = new Contracts();
            Boolean bStatus = false;

            var parent = Task.Factory.StartNew(() =>
            {
                Console.WriteLine("Parent task started...");
                foreach (var item in cbIntProductionScanFile.Items)
                {
                    RESTManifest rm = new RESTManifest();
                    ContractManifest cm = new ContractManifest();
                    APPWebServiceManifest nm = new APPWebServiceManifest();

                    cm.OutputContractFileName = @"C:\scratch\XYZFILE01\Consumer Files";

//                    rm.baseUri = "localhost:62541";
                    rm.baseUri = "xyzweb01:8080";
                    rm.uriResource = "api/ContractInfo";

                    nm.DBServer = "TESTSQL";
//                    nm.DBServer = "XYZSQL";
                    nm.Database = "XYZ Financial";
                    nm.APPDBUsername = "sa";
                    //                    nm.APPDBPassword = "Finance2345";
                    nm.APPDBPassword = "13Qeadzc";
                    nm.APPUsername = "admin";
                    nm.APPPassword = "Finance1234";

                    cm.InputContractFileName = cbIntProductionScanFile.SelectedItem.ToString();

                    var child = Task.Factory.StartNew(() =>
                    {
                        Console.WriteLine("Parent - orphan child {0}", Thread.CurrentThread.ManagedThreadId);
                        cs.processFinanceContract(rm, cm, nm, emailCommon);
                        //                     bStatus = cs.processFinanceContract(rm, cm);
                    });
                }
            });

            parent.Wait();
            Console.WriteLine("Parent task completed.");


            if (bStatus)
            {
                lblProductionTestFileStatus.Text = "Process failed.";
            }
            else
            {
                lblProductionTestFileStatus.Text = "Process passed.";
            }
        }


        private string[] getContractList()
        {
            string[] listContracts = new string[cbIntProductionScanFile.Items.Count];
            cbIntProductionScanFile.Items.CopyTo(listContracts, 0);

            return listContracts;
        }

        private void buttonProdStampMetaDataALL_Click(object sender, EventArgs e)
        {
            EmailCommon emailCommon = new EmailCommon();
            Contracts cs = new Contracts();
            string[] listOfContracts = Directory.GetFiles(@"C:\scratch\XYZFILE01\Scans\Contracts2Process");
            DateTime dtStart = DateTime.Now;
            DateTime dtEnd;
            tbStartTime.Text = dtStart.ToLongTimeString();

            Parallel.ForEach<string>(listOfContracts, (currentContract) =>
                                {
                                    RESTManifest rm = new RESTManifest();
                                    ContractManifest cm = new ContractManifest();
                                    APPWebServiceManifest nm = new APPWebServiceManifest();

                                    cm.OutputContractFileName = @"C:\scratch\XYZFILE01\Consumer Files";

//                                    rm.baseUri = "localhost:62541";
                                    rm.baseUri = "xyzweb01:8080";
                                    rm.uriResource = "api/ContractInfo";

//                                    nm.DBServer = "XYZSQL";
                                    nm.DBServer = "TESTSQL";
                                    nm.Database = "XYZ Financial";
                                    nm.APPDBUsername = "sa";
                                    nm.APPDBPassword = "13Qeadzc";
//                                    nm.APPDBPassword = "Finance2345";
                                    nm.APPUsername = "admin";
                                    nm.APPPassword = "Finance1234";

                                    cm.InputContractFileName = (currentContract);

                                    cs.processFinanceContract(rm, cm, nm, emailCommon);
                                    
                                    //if (cs.processFinanceContract(rm, cm))
                                    //{
                                    //        lblProductionTestFileStatus.Text = "Process failed.";
                                    //}
                                    //else
                                    //{
                                    //    lblProductionTestFileStatus.Text = "Process passed.";
                                    //}
                                });
            dtEnd = DateTime.Now;
            TimeSpan tsDuration = dtEnd.Subtract(dtStart);
            tbEndTime.Text = dtEnd.ToLongTimeString();
            tbDurationSEC.Text = tsDuration.Seconds.ToString();
            tbDurationMS.Text = tsDuration.Milliseconds.ToString();
            lblParallelProcessStatus.Text = "COMPLETED.";
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadScannedFileList();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            Execute();
        }

        /// <summary>
        /// Execute a Soap WebService call
        /// </summary>
        private void Execute()
        {
            HttpWebRequest request = CreateWebRequest();
            tbRequest.Text = request.RequestUri.ToString();
            XmlDocument soapEnvelopeXml = new XmlDocument();
            //            soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
            //<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
            //<soap:Body>
            //    <HelloWorld3 xmlns=""http://tempuri.org/"">
            //        <parameter1>test</parameter1>
            //        <parameter2>23</parameter2>
            //        <parameter3>test</parameter3>
            //    </HelloWorld3>
            //</soap:Body>
            //</soap:Envelope>");

            soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8"" ?>
   <soap12:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap12=""http://www.w3.org/2003/05/soap-envelope"">
   <soap12:Body>
   <ImportXMLUsingDBAuthentication xmlns = ""http://nortridge.com/"">
        <ServerName>TESTSQL</ServerName>
        <DatabaseName>XYZ Financial</DatabaseName>
        <db_username>sa</db_username>
        <db_password>13Qeadzc</db_password>
        <app_username>admin</app_username>
        <app_password>Finance1234</app_password>
        <ImportString><APP CommitBlock=""0"" EnforceTagExistence=""1""><LOAN UpdateFlag=""1"" LoanNumber=""1143321""><LOANDETAIL2 UserDefined19=""1"" /><LOANCOMMENTS Comment=""Contract Scanned"" CommentDescription=""Contract Original Scanned to consumers folder""/></LOAN></APP></ImportString>
    </ImportXMLUsingDBAuthentication>
    </soap12:Body>
     </soap12:Envelope>");

//                        < APP CommitBlock = ""0"" EnforceTagExistence = ""1"" >
//                <LOAN UpdateFlag=""1"" LoanNumber=""40836""><LOANDETAIL2 UserDefined19 = ""1"" /> 
//                       < LOANCOMMENTS Comment = ""Contract Scanned"" CommentDescription = ""Contract Original Scanned to consumers folder"" />
//                </ LOAN >
//</ APP >


            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }

            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                    {
                        string soapResult = rd.ReadToEnd();
                        tbResponse.Text = soapResult;
                    }
                }
            }
            catch (FaultException fe)
            {
                tbResponse.Text = fe.Message;
            }
            catch (ProtocolViolationException pve)
            {
                System.IO.Stream stream = (pve.InnerException as WebException).Response.GetResponseStream();
                System.Xml.XmlReader xmr = System.Xml.XmlReader.Create(stream);
                System.ServiceModel.Channels.Message message = System.ServiceModel.Channels.Message.CreateMessage(xmr, (int)stream.Length, MessageVersion.Soap12);
                MessageFault mf = MessageFault.CreateFault(message, (int)stream.Length);
                FaultException fe = new FaultException(mf);
                message.Close();
                tbResponse.Text += fe.Message;
            }
            catch (CommunicationException ce)
            {
                tbResponse.Text = ce.Message;
            }
            catch (Exception e)
            {
                tbResponse.Text = e.Message;
            }
        }

        /// <summary>
        /// Create a soap webrequest to [Url]
        /// </summary>
        /// <returns></returns>
        public HttpWebRequest CreateWebRequest()
        {
//            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@"http://dev.nl/Rvl.Demo.TestWcfServiceApplication/SoapWebService.asmx");
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@"http://xyzwebsvc01/appwebservice/service.asmx");
            //            webRequest.Headers.Add(@"SOAP:Action");
            webRequest.Headers.Add("SOAPAction", "http://nortridge.com/ImportXMLUsingDBAuthentication");
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            //            webRequest.ContentType = "application/soap+xml; charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }

    }
}
