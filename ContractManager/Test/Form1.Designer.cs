﻿namespace Test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.tbIntTestScanFile = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.tcSoap = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lblDevelopmentTestFileStatus = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.tbDurationMS = new System.Windows.Forms.TextBox();
            this.lblDuration = new System.Windows.Forms.Label();
            this.tbDurationSEC = new System.Windows.Forms.TextBox();
            this.tbEndTime = new System.Windows.Forms.TextBox();
            this.tbStartTime = new System.Windows.Forms.TextBox();
            this.lblStart = new System.Windows.Forms.Label();
            this.lblEnd = new System.Windows.Forms.Label();
            this.lblParallelProcessStatus = new System.Windows.Forms.Label();
            this.buttonProdStampMetaDataALL = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cbIntProductionScanFile = new System.Windows.Forms.ComboBox();
            this.buttonProdStampMetaData = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.lblProductionTestFileStatus = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.buttonTest6 = new System.Windows.Forms.Button();
            this.buttonTest5 = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.tbResult = new System.Windows.Forms.TextBox();
            this.tbTest4 = new System.Windows.Forms.TextBox();
            this.tbTest3 = new System.Windows.Forms.TextBox();
            this.tbTest2 = new System.Windows.Forms.TextBox();
            this.tbTest1 = new System.Windows.Forms.TextBox();
            this.buttonTest3 = new System.Windows.Forms.Button();
            this.buttonTest4 = new System.Windows.Forms.Button();
            this.buttonTest2 = new System.Windows.Forms.Button();
            this.buttonTest1 = new System.Windows.Forms.Button();
            this.tbSearchFolder = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.tbRESTResult = new System.Windows.Forms.TextBox();
            this.tbLoanNumber = new System.Windows.Forms.TextBox();
            this.buttonREST = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.lblConsole = new System.Windows.Forms.Label();
            this.tbConsole = new System.Windows.Forms.TextBox();
            this.tbThread3 = new System.Windows.Forms.TextBox();
            this.tbThread2 = new System.Windows.Forms.TextBox();
            this.lblThread4 = new System.Windows.Forms.Label();
            this.lblThread3 = new System.Windows.Forms.Label();
            this.lblThread2 = new System.Windows.Forms.Label();
            this.lblThread1 = new System.Windows.Forms.Label();
            this.tbThread4 = new System.Windows.Forms.TextBox();
            this.tbThread1 = new System.Windows.Forms.TextBox();
            this.btnGOThreads = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tbResponse = new System.Windows.Forms.TextBox();
            this.tbRequest = new System.Windows.Forms.TextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.tcSoap.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("HP Simplified W01 Bold Italic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(695, 50);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(156, 85);
            this.button1.TabIndex = 0;
            this.button1.Text = "Stamp !!!";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbIntTestScanFile
            // 
            this.tbIntTestScanFile.Font = new System.Drawing.Font("HP Simplified", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbIntTestScanFile.Location = new System.Drawing.Point(179, 17);
            this.tbIntTestScanFile.Name = "tbIntTestScanFile";
            this.tbIntTestScanFile.Size = new System.Drawing.Size(834, 26);
            this.tbIntTestScanFile.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("HP Simplified", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Green;
            this.label1.Location = new System.Drawing.Point(12, 428);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 15);
            this.label1.TabIndex = 2;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("HP Simplified W01 Bold Italic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(179, 129);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(156, 85);
            this.button2.TabIndex = 3;
            this.button2.Text = "Watermark !!!";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("HP Simplified W01 Bold Italic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(857, 49);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(156, 85);
            this.button3.TabIndex = 3;
            this.button3.Text = "Insert TEXT !!! Upper right corner";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("HP Simplified W01 Bold Italic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(695, 140);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(318, 85);
            this.button4.TabIndex = 3;
            this.button4.Text = "Add stamp and metadata !!!";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // tcSoap
            // 
            this.tcSoap.Controls.Add(this.tabPage1);
            this.tcSoap.Controls.Add(this.tabPage2);
            this.tcSoap.Controls.Add(this.tabPage3);
            this.tcSoap.Controls.Add(this.tabPage4);
            this.tcSoap.Controls.Add(this.tabPage5);
            this.tcSoap.Location = new System.Drawing.Point(12, 12);
            this.tcSoap.Name = "tcSoap";
            this.tcSoap.SelectedIndex = 0;
            this.tcSoap.Size = new System.Drawing.Size(1053, 626);
            this.tcSoap.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lblDevelopmentTestFileStatus);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.button4);
            this.tabPage1.Controls.Add(this.tbIntTestScanFile);
            this.tabPage1.Controls.Add(this.button3);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1045, 600);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "PDF File processing";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // lblDevelopmentTestFileStatus
            // 
            this.lblDevelopmentTestFileStatus.AutoSize = true;
            this.lblDevelopmentTestFileStatus.Location = new System.Drawing.Point(60, 256);
            this.lblDevelopmentTestFileStatus.Name = "lblDevelopmentTestFileStatus";
            this.lblDevelopmentTestFileStatus.Size = new System.Drawing.Size(53, 13);
            this.lblDevelopmentTestFileStatus.TabIndex = 9;
            this.lblDevelopmentTestFileStatus.Text = "...status...";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(60, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Development Test File";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.AntiqueWhite;
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.tbDurationMS);
            this.panel1.Controls.Add(this.lblDuration);
            this.panel1.Controls.Add(this.tbDurationSEC);
            this.panel1.Controls.Add(this.tbEndTime);
            this.panel1.Controls.Add(this.tbStartTime);
            this.panel1.Controls.Add(this.lblStart);
            this.panel1.Controls.Add(this.lblEnd);
            this.panel1.Controls.Add(this.lblParallelProcessStatus);
            this.panel1.Controls.Add(this.buttonProdStampMetaDataALL);
            this.panel1.Location = new System.Drawing.Point(14, 497);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1025, 96);
            this.panel1.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("HP Simplified", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(873, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 21);
            this.label5.TabIndex = 20;
            this.label5.Text = "ms:";
            // 
            // tbDurationMS
            // 
            this.tbDurationMS.Location = new System.Drawing.Point(913, 58);
            this.tbDurationMS.Name = "tbDurationMS";
            this.tbDurationMS.Size = new System.Drawing.Size(100, 20);
            this.tbDurationMS.TabIndex = 19;
            // 
            // lblDuration
            // 
            this.lblDuration.AutoSize = true;
            this.lblDuration.Font = new System.Drawing.Font("HP Simplified", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDuration.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblDuration.Location = new System.Drawing.Point(619, 57);
            this.lblDuration.Name = "lblDuration";
            this.lblDuration.Size = new System.Drawing.Size(135, 21);
            this.lblDuration.TabIndex = 18;
            this.lblDuration.Text = "Duration(seconds):";
            // 
            // tbDurationSEC
            // 
            this.tbDurationSEC.Location = new System.Drawing.Point(770, 58);
            this.tbDurationSEC.Name = "tbDurationSEC";
            this.tbDurationSEC.Size = new System.Drawing.Size(100, 20);
            this.tbDurationSEC.TabIndex = 17;
            // 
            // tbEndTime
            // 
            this.tbEndTime.Location = new System.Drawing.Point(769, 32);
            this.tbEndTime.Name = "tbEndTime";
            this.tbEndTime.Size = new System.Drawing.Size(100, 20);
            this.tbEndTime.TabIndex = 16;
            // 
            // tbStartTime
            // 
            this.tbStartTime.Location = new System.Drawing.Point(770, 8);
            this.tbStartTime.Name = "tbStartTime";
            this.tbStartTime.Size = new System.Drawing.Size(100, 20);
            this.tbStartTime.TabIndex = 15;
            // 
            // lblStart
            // 
            this.lblStart.AutoSize = true;
            this.lblStart.Font = new System.Drawing.Font("HP Simplified", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStart.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblStart.Location = new System.Drawing.Point(706, 7);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(48, 21);
            this.lblStart.TabIndex = 14;
            this.lblStart.Text = "Start:";
            // 
            // lblEnd
            // 
            this.lblEnd.AutoSize = true;
            this.lblEnd.Font = new System.Drawing.Font("HP Simplified", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnd.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblEnd.Location = new System.Drawing.Point(716, 32);
            this.lblEnd.Name = "lblEnd";
            this.lblEnd.Size = new System.Drawing.Size(38, 21);
            this.lblEnd.TabIndex = 13;
            this.lblEnd.Text = "End:";
            // 
            // lblParallelProcessStatus
            // 
            this.lblParallelProcessStatus.AutoSize = true;
            this.lblParallelProcessStatus.Font = new System.Drawing.Font("HP Simplified", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblParallelProcessStatus.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblParallelProcessStatus.Location = new System.Drawing.Point(521, 72);
            this.lblParallelProcessStatus.Name = "lblParallelProcessStatus";
            this.lblParallelProcessStatus.Size = new System.Drawing.Size(76, 21);
            this.lblParallelProcessStatus.TabIndex = 12;
            this.lblParallelProcessStatus.Text = "...status...";
            // 
            // buttonProdStampMetaDataALL
            // 
            this.buttonProdStampMetaDataALL.Font = new System.Drawing.Font("HP Simplified W01 Bold Italic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonProdStampMetaDataALL.Location = new System.Drawing.Point(165, 8);
            this.buttonProdStampMetaDataALL.Name = "buttonProdStampMetaDataALL";
            this.buttonProdStampMetaDataALL.Size = new System.Drawing.Size(304, 85);
            this.buttonProdStampMetaDataALL.TabIndex = 11;
            this.buttonProdStampMetaDataALL.Text = "Process ALL\r\n(Add stamp and metadata)";
            this.buttonProdStampMetaDataALL.UseVisualStyleBackColor = true;
            this.buttonProdStampMetaDataALL.Click += new System.EventHandler(this.buttonProdStampMetaDataALL_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.AntiqueWhite;
            this.panel2.Location = new System.Drawing.Point(14, 7);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1025, 282);
            this.panel2.TabIndex = 13;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.AntiqueWhite;
            this.panel3.Controls.Add(this.cbIntProductionScanFile);
            this.panel3.Controls.Add(this.buttonProdStampMetaData);
            this.panel3.Controls.Add(this.btnRefresh);
            this.panel3.Controls.Add(this.lblProductionTestFileStatus);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(14, 296);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1025, 193);
            this.panel3.TabIndex = 14;
            // 
            // cbIntProductionScanFile
            // 
            this.cbIntProductionScanFile.Font = new System.Drawing.Font("HP Simplified", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbIntProductionScanFile.FormattingEnabled = true;
            this.cbIntProductionScanFile.Location = new System.Drawing.Point(132, 16);
            this.cbIntProductionScanFile.Name = "cbIntProductionScanFile";
            this.cbIntProductionScanFile.Size = new System.Drawing.Size(820, 29);
            this.cbIntProductionScanFile.TabIndex = 10;
            // 
            // buttonProdStampMetaData
            // 
            this.buttonProdStampMetaData.Font = new System.Drawing.Font("HP Simplified W01 Bold Italic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonProdStampMetaData.Location = new System.Drawing.Point(132, 51);
            this.buttonProdStampMetaData.Name = "buttonProdStampMetaData";
            this.buttonProdStampMetaData.Size = new System.Drawing.Size(318, 85);
            this.buttonProdStampMetaData.TabIndex = 7;
            this.buttonProdStampMetaData.Text = "Process a single\r\n(Add stamp and metadata)";
            this.buttonProdStampMetaData.UseVisualStyleBackColor = true;
            this.buttonProdStampMetaData.Click += new System.EventHandler(this.buttonProdStampMetaData_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(956, 5);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(66, 40);
            this.btnRefresh.TabIndex = 9;
            this.btnRefresh.Text = "Refresh List";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // lblProductionTestFileStatus
            // 
            this.lblProductionTestFileStatus.AutoSize = true;
            this.lblProductionTestFileStatus.Location = new System.Drawing.Point(35, 167);
            this.lblProductionTestFileStatus.Name = "lblProductionTestFileStatus";
            this.lblProductionTestFileStatus.Size = new System.Drawing.Size(53, 13);
            this.lblProductionTestFileStatus.TabIndex = 8;
            this.lblProductionTestFileStatus.Text = "...status...";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Production Test File";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.buttonTest6);
            this.tabPage2.Controls.Add(this.buttonTest5);
            this.tabPage2.Controls.Add(this.textBox3);
            this.tabPage2.Controls.Add(this.textBox2);
            this.tabPage2.Controls.Add(this.tbResult);
            this.tabPage2.Controls.Add(this.tbTest4);
            this.tabPage2.Controls.Add(this.tbTest3);
            this.tabPage2.Controls.Add(this.tbTest2);
            this.tabPage2.Controls.Add(this.tbTest1);
            this.tabPage2.Controls.Add(this.buttonTest3);
            this.tabPage2.Controls.Add(this.buttonTest4);
            this.tabPage2.Controls.Add(this.buttonTest2);
            this.tabPage2.Controls.Add(this.buttonTest1);
            this.tabPage2.Controls.Add(this.tbSearchFolder);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1045, 600);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "DirectorySearch";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // buttonTest6
            // 
            this.buttonTest6.Location = new System.Drawing.Point(892, 272);
            this.buttonTest6.Name = "buttonTest6";
            this.buttonTest6.Size = new System.Drawing.Size(138, 32);
            this.buttonTest6.TabIndex = 13;
            this.buttonTest6.Text = "Search";
            this.buttonTest6.UseVisualStyleBackColor = true;
            this.buttonTest6.Click += new System.EventHandler(this.buttonTest6_Click);
            // 
            // buttonTest5
            // 
            this.buttonTest5.Location = new System.Drawing.Point(892, 234);
            this.buttonTest5.Name = "buttonTest5";
            this.buttonTest5.Size = new System.Drawing.Size(138, 32);
            this.buttonTest5.TabIndex = 12;
            this.buttonTest5.Text = "Search";
            this.buttonTest5.UseVisualStyleBackColor = true;
            this.buttonTest5.Click += new System.EventHandler(this.buttonTest5_Click);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(300, 271);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(563, 20);
            this.textBox3.TabIndex = 11;
            this.textBox3.Text = "BACANI, JEAN-VICTORIA SM189";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(300, 234);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(563, 20);
            this.textBox2.TabIndex = 10;
            this.textBox2.Text = "BACANI, JEAN VICTORIA SM189";
            // 
            // tbResult
            // 
            this.tbResult.Location = new System.Drawing.Point(165, 330);
            this.tbResult.Multiline = true;
            this.tbResult.Name = "tbResult";
            this.tbResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbResult.Size = new System.Drawing.Size(865, 189);
            this.tbResult.TabIndex = 9;
            // 
            // tbTest4
            // 
            this.tbTest4.Location = new System.Drawing.Point(300, 193);
            this.tbTest4.Name = "tbTest4";
            this.tbTest4.Size = new System.Drawing.Size(563, 20);
            this.tbTest4.TabIndex = 8;
            this.tbTest4.Text = "BABERS, ROSE FQ130";
            // 
            // tbTest3
            // 
            this.tbTest3.Location = new System.Drawing.Point(300, 156);
            this.tbTest3.Name = "tbTest3";
            this.tbTest3.Size = new System.Drawing.Size(563, 20);
            this.tbTest3.TabIndex = 7;
            this.tbTest3.Text = "BABCOCK, JENNIFER & JOHNSON, WARREN FQ117";
            // 
            // tbTest2
            // 
            this.tbTest2.Location = new System.Drawing.Point(300, 120);
            this.tbTest2.Name = "tbTest2";
            this.tbTest2.Size = new System.Drawing.Size(563, 20);
            this.tbTest2.TabIndex = 6;
            this.tbTest2.Text = "BABB, ROBERT & BABB, CAROLYN RC133";
            // 
            // tbTest1
            // 
            this.tbTest1.Location = new System.Drawing.Point(300, 79);
            this.tbTest1.Name = "tbTest1";
            this.tbTest1.Size = new System.Drawing.Size(563, 20);
            this.tbTest1.TabIndex = 5;
            this.tbTest1.Text = "BABB, CHELSEA & DALTON RC265";
            // 
            // buttonTest3
            // 
            this.buttonTest3.Location = new System.Drawing.Point(892, 158);
            this.buttonTest3.Name = "buttonTest3";
            this.buttonTest3.Size = new System.Drawing.Size(138, 32);
            this.buttonTest3.TabIndex = 4;
            this.buttonTest3.Text = "Search";
            this.buttonTest3.UseVisualStyleBackColor = true;
            this.buttonTest3.Click += new System.EventHandler(this.buttonTest3_Click);
            // 
            // buttonTest4
            // 
            this.buttonTest4.Location = new System.Drawing.Point(892, 196);
            this.buttonTest4.Name = "buttonTest4";
            this.buttonTest4.Size = new System.Drawing.Size(138, 32);
            this.buttonTest4.TabIndex = 3;
            this.buttonTest4.Text = "Search";
            this.buttonTest4.UseVisualStyleBackColor = true;
            this.buttonTest4.Click += new System.EventHandler(this.buttonTest4_Click);
            // 
            // buttonTest2
            // 
            this.buttonTest2.Location = new System.Drawing.Point(892, 120);
            this.buttonTest2.Name = "buttonTest2";
            this.buttonTest2.Size = new System.Drawing.Size(138, 32);
            this.buttonTest2.TabIndex = 2;
            this.buttonTest2.Text = "Search";
            this.buttonTest2.UseVisualStyleBackColor = true;
            this.buttonTest2.Click += new System.EventHandler(this.buttonTest2_Click);
            // 
            // buttonTest1
            // 
            this.buttonTest1.Location = new System.Drawing.Point(892, 79);
            this.buttonTest1.Name = "buttonTest1";
            this.buttonTest1.Size = new System.Drawing.Size(138, 32);
            this.buttonTest1.TabIndex = 1;
            this.buttonTest1.Text = "Search";
            this.buttonTest1.UseVisualStyleBackColor = true;
            this.buttonTest1.Click += new System.EventHandler(this.buttonTest1_Click);
            // 
            // tbSearchFolder
            // 
            this.tbSearchFolder.Location = new System.Drawing.Point(32, 31);
            this.tbSearchFolder.Name = "tbSearchFolder";
            this.tbSearchFolder.Size = new System.Drawing.Size(920, 20);
            this.tbSearchFolder.TabIndex = 0;
            this.tbSearchFolder.Text = "\\\\xyzfile01\\Consumer Files";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.tbRESTResult);
            this.tabPage3.Controls.Add(this.tbLoanNumber);
            this.tabPage3.Controls.Add(this.buttonREST);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1045, 600);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "REST WS ";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Loan Number";
            // 
            // tbRESTResult
            // 
            this.tbRESTResult.Location = new System.Drawing.Point(54, 121);
            this.tbRESTResult.Multiline = true;
            this.tbRESTResult.Name = "tbRESTResult";
            this.tbRESTResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbRESTResult.Size = new System.Drawing.Size(918, 181);
            this.tbRESTResult.TabIndex = 2;
            // 
            // tbLoanNumber
            // 
            this.tbLoanNumber.Location = new System.Drawing.Point(140, 37);
            this.tbLoanNumber.Name = "tbLoanNumber";
            this.tbLoanNumber.Size = new System.Drawing.Size(173, 20);
            this.tbLoanNumber.TabIndex = 1;
            // 
            // buttonREST
            // 
            this.buttonREST.Location = new System.Drawing.Point(336, 35);
            this.buttonREST.Name = "buttonREST";
            this.buttonREST.Size = new System.Drawing.Size(75, 23);
            this.buttonREST.TabIndex = 0;
            this.buttonREST.Text = "Get Loan Info";
            this.buttonREST.UseVisualStyleBackColor = true;
            this.buttonREST.Click += new System.EventHandler(this.buttonREST_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.lblConsole);
            this.tabPage4.Controls.Add(this.tbConsole);
            this.tabPage4.Controls.Add(this.tbThread3);
            this.tabPage4.Controls.Add(this.tbThread2);
            this.tabPage4.Controls.Add(this.lblThread4);
            this.tabPage4.Controls.Add(this.lblThread3);
            this.tabPage4.Controls.Add(this.lblThread2);
            this.tabPage4.Controls.Add(this.lblThread1);
            this.tabPage4.Controls.Add(this.tbThread4);
            this.tabPage4.Controls.Add(this.tbThread1);
            this.tabPage4.Controls.Add(this.btnGOThreads);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1045, 600);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "ThreadTest";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // lblConsole
            // 
            this.lblConsole.AutoSize = true;
            this.lblConsole.Location = new System.Drawing.Point(13, 276);
            this.lblConsole.Name = "lblConsole";
            this.lblConsole.Size = new System.Drawing.Size(72, 13);
            this.lblConsole.TabIndex = 10;
            this.lblConsole.Text = "Task Console";
            // 
            // tbConsole
            // 
            this.tbConsole.Location = new System.Drawing.Point(16, 292);
            this.tbConsole.Multiline = true;
            this.tbConsole.Name = "tbConsole";
            this.tbConsole.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbConsole.Size = new System.Drawing.Size(1023, 227);
            this.tbConsole.TabIndex = 9;
            // 
            // tbThread3
            // 
            this.tbThread3.Location = new System.Drawing.Point(228, 156);
            this.tbThread3.Name = "tbThread3";
            this.tbThread3.Size = new System.Drawing.Size(100, 20);
            this.tbThread3.TabIndex = 8;
            // 
            // tbThread2
            // 
            this.tbThread2.Location = new System.Drawing.Point(228, 103);
            this.tbThread2.Name = "tbThread2";
            this.tbThread2.Size = new System.Drawing.Size(100, 20);
            this.tbThread2.TabIndex = 7;
            // 
            // lblThread4
            // 
            this.lblThread4.AutoSize = true;
            this.lblThread4.Location = new System.Drawing.Point(103, 209);
            this.lblThread4.Name = "lblThread4";
            this.lblThread4.Size = new System.Drawing.Size(50, 13);
            this.lblThread4.TabIndex = 6;
            this.lblThread4.Text = "Thread 4";
            // 
            // lblThread3
            // 
            this.lblThread3.AutoSize = true;
            this.lblThread3.Location = new System.Drawing.Point(103, 156);
            this.lblThread3.Name = "lblThread3";
            this.lblThread3.Size = new System.Drawing.Size(50, 13);
            this.lblThread3.TabIndex = 5;
            this.lblThread3.Text = "Thread 3";
            // 
            // lblThread2
            // 
            this.lblThread2.AutoSize = true;
            this.lblThread2.Location = new System.Drawing.Point(103, 103);
            this.lblThread2.Name = "lblThread2";
            this.lblThread2.Size = new System.Drawing.Size(50, 13);
            this.lblThread2.TabIndex = 4;
            this.lblThread2.Text = "Thread 2";
            // 
            // lblThread1
            // 
            this.lblThread1.AutoSize = true;
            this.lblThread1.Location = new System.Drawing.Point(103, 50);
            this.lblThread1.Name = "lblThread1";
            this.lblThread1.Size = new System.Drawing.Size(50, 13);
            this.lblThread1.TabIndex = 3;
            this.lblThread1.Text = "Thread 1";
            // 
            // tbThread4
            // 
            this.tbThread4.Location = new System.Drawing.Point(228, 209);
            this.tbThread4.Name = "tbThread4";
            this.tbThread4.Size = new System.Drawing.Size(100, 20);
            this.tbThread4.TabIndex = 2;
            // 
            // tbThread1
            // 
            this.tbThread1.Location = new System.Drawing.Point(228, 50);
            this.tbThread1.Name = "tbThread1";
            this.tbThread1.Size = new System.Drawing.Size(100, 20);
            this.tbThread1.TabIndex = 1;
            // 
            // btnGOThreads
            // 
            this.btnGOThreads.Location = new System.Drawing.Point(424, 50);
            this.btnGOThreads.Name = "btnGOThreads";
            this.btnGOThreads.Size = new System.Drawing.Size(75, 23);
            this.btnGOThreads.TabIndex = 0;
            this.btnGOThreads.Text = "GO!";
            this.btnGOThreads.UseVisualStyleBackColor = true;
            this.btnGOThreads.Click += new System.EventHandler(this.btnGOThreads_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.tbResponse);
            this.tabPage5.Controls.Add(this.tbRequest);
            this.tabPage5.Controls.Add(this.btnSend);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1045, 600);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "SOAP Client";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tbResponse
            // 
            this.tbResponse.Location = new System.Drawing.Point(28, 227);
            this.tbResponse.Multiline = true;
            this.tbResponse.Name = "tbResponse";
            this.tbResponse.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbResponse.Size = new System.Drawing.Size(890, 158);
            this.tbResponse.TabIndex = 2;
            // 
            // tbRequest
            // 
            this.tbRequest.Location = new System.Drawing.Point(28, 6);
            this.tbRequest.Multiline = true;
            this.tbRequest.Name = "tbRequest";
            this.tbRequest.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbRequest.Size = new System.Drawing.Size(890, 158);
            this.tbRequest.TabIndex = 1;
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(843, 184);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(75, 23);
            this.btnSend.TabIndex = 0;
            this.btnSend.Text = "Execute";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1077, 650);
            this.Controls.Add(this.tcSoap);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tcSoap.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox tbIntTestScanFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TabControl tcSoap;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox tbResult;
        private System.Windows.Forms.TextBox tbTest4;
        private System.Windows.Forms.TextBox tbTest3;
        private System.Windows.Forms.TextBox tbTest2;
        private System.Windows.Forms.TextBox tbTest1;
        private System.Windows.Forms.Button buttonTest3;
        private System.Windows.Forms.Button buttonTest4;
        private System.Windows.Forms.Button buttonTest2;
        private System.Windows.Forms.Button buttonTest1;
        private System.Windows.Forms.TextBox tbSearchFolder;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button buttonTest6;
        private System.Windows.Forms.Button buttonTest5;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbRESTResult;
        private System.Windows.Forms.TextBox tbLoanNumber;
        private System.Windows.Forms.Button buttonREST;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonProdStampMetaData;
        private System.Windows.Forms.Label lblDevelopmentTestFileStatus;
        private System.Windows.Forms.Label lblProductionTestFileStatus;
        private System.Windows.Forms.ComboBox cbIntProductionScanFile;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox tbThread3;
        private System.Windows.Forms.TextBox tbThread2;
        private System.Windows.Forms.Label lblThread4;
        private System.Windows.Forms.Label lblThread3;
        private System.Windows.Forms.Label lblThread2;
        private System.Windows.Forms.Label lblThread1;
        private System.Windows.Forms.TextBox tbThread4;
        private System.Windows.Forms.TextBox tbThread1;
        private System.Windows.Forms.Button btnGOThreads;
        private System.Windows.Forms.Label lblConsole;
        private System.Windows.Forms.TextBox tbConsole;
        private System.Windows.Forms.Button buttonProdStampMetaDataALL;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblParallelProcessStatus;
        private System.Windows.Forms.Label lblDuration;
        private System.Windows.Forms.TextBox tbDurationSEC;
        private System.Windows.Forms.TextBox tbEndTime;
        private System.Windows.Forms.TextBox tbStartTime;
        private System.Windows.Forms.Label lblStart;
        private System.Windows.Forms.Label lblEnd;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TextBox tbResponse;
        private System.Windows.Forms.TextBox tbRequest;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbDurationMS;
    }
}

