﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

//https://msdn.microsoft.com/en-us/library/zt39148a(v=vs.110).aspx

//"C:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe"

namespace ContractManager
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new ContractService(args)
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}

