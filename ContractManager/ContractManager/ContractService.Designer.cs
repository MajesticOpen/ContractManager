﻿namespace ContractManager
{
    partial class ContractService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.eventLog1 = new System.Diagnostics.EventLog();
            this.fileSystemWatcherContracts = new System.IO.FileSystemWatcher();
            ((System.ComponentModel.ISupportInitialize)(this.eventLog1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcherContracts)).BeginInit();
            // 
            // fileSystemWatcherContracts
            // 
            this.fileSystemWatcherContracts.EnableRaisingEvents = true;
            this.fileSystemWatcherContracts.Changed += new System.IO.FileSystemEventHandler(this.fileSystemWatcherContracts_Changed);
            this.fileSystemWatcherContracts.Created += new System.IO.FileSystemEventHandler(this.fileSystemWatcherContracts_Created);
            this.fileSystemWatcherContracts.Deleted += new System.IO.FileSystemEventHandler(this.fileSystemWatcherContracts_Deleted);
            this.fileSystemWatcherContracts.Renamed += new System.IO.RenamedEventHandler(this.fileSystemWatcherContracts_Renamed);
            // 
            // ContractService
            // 
            this.CanPauseAndContinue = true;
            this.CanShutdown = true;
            this.ServiceName = "XYZContractServices";
            ((System.ComponentModel.ISupportInitialize)(this.eventLog1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcherContracts)).EndInit();

        }

        #endregion

        private System.Diagnostics.EventLog eventLog1;
        private System.IO.FileSystemWatcher fileSystemWatcherContracts;
    }
}
