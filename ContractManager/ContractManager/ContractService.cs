﻿using System;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Runtime.InteropServices;
using System.Configuration;
using XYZContractServices;
using System.IO;
using System.Threading.Tasks;

namespace ContractManager
{
    public partial class ContractService : ServiceBase
    {
        private System.Diagnostics.EventLog eventLog;

        public int eventId { get; private set; }

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(IntPtr handle, ref ServiceStatus serviceStatus);

        public enum ServiceState
        {
            SERVICE_STOPPED = 0x00000001,
            SERVICE_START_PENDING = 0x00000002,
            SERVICE_STOP_PENDING = 0x00000003,
            SERVICE_RUNNING = 0x00000004,
            SERVICE_CONTINUE_PENDING = 0x00000005,
            SERVICE_PAUSE_PENDING = 0x00000006,
            SERVICE_PAUSED = 0x00000007,
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct ServiceStatus
        {
            public long dwServiceType;
            public ServiceState dwCurrentState;
            public long dwControlsAccepted;
            public long dwWin32ExitCode;
            public long dwServiceSpecificExitCode;
            public long dwCheckPoint;
            public long dwWaitHint;
        };

        public ContractService(string[] args)
        {
            InitializeComponent();
            string eventSourceName = "XYZ";
            string logName = "ContractServices";
            CanPauseAndContinue = true;                                                             //Set this to false if pause/continue is unnecessary
            Contracts contracts = new Contracts();

            if (args.Count() > 0)
            {
                eventSourceName = args[0];
            }
            if (args.Count() > 1)
            {
                logName = args[1];
            }

            eventLog = new EventLog();
            if (!EventLog.SourceExists(eventSourceName))
            {
                EventLog.CreateEventSource(eventSourceName, logName);
            }
            eventLog.Source = eventSourceName;
            eventLog.Log = logName; 
        }

        protected override void OnStart(string[] args)
        {
            string folderContracts = ConfigurationManager.AppSettings[@"ScanContractsFolder"];
            eventLog.WriteEntry("Starting ContractsService...");

            eventLog.WriteEntry(string.Format("(BEFORE)Watching for incoming contracts at location: {0}", folderContracts));
            fileSystemWatcherContracts.Path = folderContracts;
            eventLog.WriteEntry(string.Format("Watching for incoming contracts at location: {0}", folderContracts));

            // Update the service state to Start Pending.
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = ServiceState.SERVICE_START_PENDING;
            serviceStatus.dwWaitHint = 100000;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            // Set the timer based on setting in the app.config
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = (Convert.ToDouble(ConfigurationManager.AppSettings[@"FileWatchTimer"]) * 1000);
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();

            // Update the service state to Running.
            serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            ProcessScanContracts(ConfigurationManager.AppSettings[@"ScanContractsFolder"]);
        }

        protected override void OnStop()
        {
            eventLog.WriteEntry("Stopping ContractsService.");
        }

        protected override void OnContinue()
        {
            base.OnContinue();
            eventLog.WriteEntry("Continuing ContractsService...");
        }

        protected override void OnPause()
        {
            base.OnPause();
            eventLog.WriteEntry("Pausing ContractsService...");
//            Thread.Sleep(1000);
        }

        protected override void OnShutdown()
        {
            base.OnShutdown();
            eventLog.WriteEntry("Shutting down ContractsService...");
        }

        private void fileSystemWatcherContracts_Changed(object sender, System.IO.FileSystemEventArgs e)
        {
            eventLog.WriteEntry(string.Format("File changed: {0}", e.FullPath), EventLogEntryType.Information, eventId++);
        }

        private void fileSystemWatcherContracts_Created(object sender, System.IO.FileSystemEventArgs e)
        {
            eventLog.WriteEntry(string.Format("File created: {0}", e.FullPath), EventLogEntryType.Information, eventId++);
        }

        private void fileSystemWatcherContracts_Deleted(object sender, System.IO.FileSystemEventArgs e)
        {
            eventLog.WriteEntry(string.Format("File deleted: {0}", e.FullPath), EventLogEntryType.Information, eventId++);
        }

        private void fileSystemWatcherContracts_Renamed(object sender, System.IO.RenamedEventArgs e)
        {
            eventLog.WriteEntry(string.Format("File renamed: {0}", e.FullPath), EventLogEntryType.Information, eventId++);
        }


        void ProcessScanContracts(string scanDocumentsFolder)
        { 
            string folderConsumerFiles = ConfigurationManager.AppSettings[@"ConsumerFiles"];
            ContractManifest cm = new ContractManifest();
            RESTManifest rm = new RESTManifest();
            APPWebServiceManifest nm = new APPWebServiceManifest();

            //Prepare the web service manifest(XYZ)
            rm.baseUri = ConfigurationManager.AppSettings["WebServiceHost"];
            rm.uriResource = ConfigurationManager.AppSettings["WebServiceContractInfoUri"];
            //Prepare the APP web service manifest
            nm.DBServer = ConfigurationManager.AppSettings["DBServer"];
            nm.Database = ConfigurationManager.AppSettings["Database"];
            nm.APPDBUsername = ConfigurationManager.AppSettings["APPDBUsername"];
            nm.APPDBPassword = ConfigurationManager.AppSettings["APPDBPassword"];
            nm.APPUsername = ConfigurationManager.AppSettings["APPUsername"];
            nm.APPPassword = ConfigurationManager.AppSettings["APPPassword"];
            //Prepare the contract manifest
            cm.OutputContractFileName = folderConsumerFiles;
            cm.ExceptionsContractFolder = ConfigurationManager.AppSettings["ConsumerRepositoryMissingFolder"];

            processInParallel(scanDocumentsFolder, rm, cm, nm);

        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="scanFolder"></param>
        /// <param name="rm"></param>
        /// <param name="cm"></param>
        /// <param name="nm"></param>
        void processInParallel(string scanFolder, RESTManifest rm, ContractManifest cm, APPWebServiceManifest nm)
        {
            Contracts cs = new Contracts();
            DateTime dtStart = DateTime.Now;
            DateTime dtEnd;
            string[] listOfContracts = Directory.GetFiles(scanFolder,"*.pdf", SearchOption.TopDirectoryOnly);
            string sStartTime = dtStart.ToLongTimeString();
            string sDurationSeconds = string.Empty;
            string sDurationMilliseconds = string.Empty;
            EmailCommon emailCommon = new EmailCommon();
            int procCount = System.Environment.ProcessorCount;

            if (listOfContracts.Count() > 0)
            {
                try
                {
                    if (listOfContracts.Length < procCount)
                    {
                        foreach (var currentContract in listOfContracts)
                        {
                            cm.InputContractFileName = (currentContract);
                            cs.processFinanceContract(rm, cm, nm, emailCommon);
                        }
                    }
                    else
                    {
                        Parallel.ForEach<string>(listOfContracts, (currentContract) =>
                        {
                            cm.InputContractFileName = (currentContract);
                            cs.processFinanceContract(rm, cm, nm, emailCommon);
                        });
                    }

                    dtEnd = DateTime.Now;
                    TimeSpan tsDuration = dtEnd.Subtract(dtStart);
                    sDurationSeconds = tsDuration.Seconds.ToString();
                    sDurationMilliseconds = tsDuration.Milliseconds.ToString();
                    eventLog.WriteEntry(string.Format("Processing complete. {0} files processed in {1} seconds / {2} milliseconds", listOfContracts.Count(), sDurationSeconds, sDurationMilliseconds), EventLogEntryType.Information, eventId++);
                }
                catch (Exception ex)
                {
                    eventLog.WriteEntry(string.Format("An exception occurred during parallel processing. Exception: {0}", ex.Message), EventLogEntryType.Error, eventId++);
                }
            }
        }


    }
}
